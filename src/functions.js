export const makeInitials = (name) => {
  let author = name.toUpperCase()
  let initials = author.search(' ')
  if( initials ){
    initials = <span className="text-white font-bold">{author.substr(0,1)}&nbsp;{author.substr(author.search(' ') + 2, 1)}</span>
  }else{
    initials = author.substr(0,1)
  }

  return initials
}