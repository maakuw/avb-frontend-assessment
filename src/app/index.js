import React from "react"

import "app/App.css"
import logo from "app/logo.svg"
import Header from "components/Header"
import CommentModal from "components/CommentModal"
import Article from "components/Article"
import Leaderboard from "components/Leaderboard"

function App() {
  return (
    <main className="max-h-screen max-w-screen flex flex-col content-stretch">
      <Header />

      <figure className="App-header w-full min-h-30vw md:min-h-30 flex flex-col items-center justify-center text-white mt-14 md:mt-16 mx-auto mb-auto bg-slate">
        <img src={logo} width="32px" height="32px" className="App-logo motion-safe:animate-spin pointer-events-none w-auto" alt="Slowly spinning atom" />
      </figure>
      
      <section className="lg:container flex self-center justify-between items-start flex-col md:flex-row px-2 md:px-6 lg:px-0 md:space-x-3 lg:space-x-8">
        <Article />
        <Leaderboard />
      </section>

      <CommentModal />
    </main>
  );
}

export default App;
