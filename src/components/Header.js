import React from "react";
import { useDispatch } from "react-redux";
//import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
//import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { openCommentsModal } from "store/slices/view";
/* These styles duplicate the existing tailwind.css flex-grow property. Since it's already included, let's use that to decrease overhead
https://tailwindcss.com/docs/flex-grow
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  title: {
    flexGrow: 1,
  },
}));
*/
const Header = () => {
//  const classes = useStyles(); Don't need this since we're leveraging tailwind for this single style
  const dispatch = useDispatch();

  const handleOpen = () => dispatch(openCommentsModal());

  return (
    <AppBar position="fixed" className="flex-grow">
      <Toolbar>
        {/* Replaced this with real DOM. You can NOT start a page with an H6. SERIOUS accessibility demerit in the score -20points
        <Typography variant="h6" className={classes.title}>
          Commentor
        </Typography>
        Using the Helper class leverages the same visual, without the DOM hit to our score. SPAN doesn't score at all, which is good!
        */}
        <span className="h6 flex-grow">Commentor</span>
        <Button color="inherit" onClick={handleOpen}>
          Add Comment
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
