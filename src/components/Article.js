import React from "react"
import { useSelector } from "react-redux";
import { Box } from "@material-ui/core"
import { makeInitials } from "functions"
import { getComments } from "store/slices/view"

const Article = () => {
  const comments = useSelector(getComments)
  return (
    <article className="w-full md:w-2/3 xl:w-3/5 order-2 md:order-1">
    { comments &&
      <Box className="w-full">
        <h1 className="py-4 mt-8 md:mt-4 pl-2 md:pl-0 lg:mt-6 mb-4 md:mb-8 text-3xl lg:text-2xl border-b text-center md:text-left">Recent Comments</h1>
        <dl className="mb-auto mt-0 mx-3 md:mr-auto md:ml-16">
          { comments.map( (comment, c) => {
            return (
              <React.Fragment key={`comment_modal__comment--${c}`}>
                <dt className="relative flex items-center">
                  <Box className="rounded-full static md:absolute w-12 h-12 flex text-bold -ml-3 md:-ml-16 mr-3 md:mr-auto flex-col justify-center items-center bg-primary-light">
                    {makeInitials(comment.name)}
                  </Box>
                  <span className="font-bold">{comment.name}</span>
                </dt>
                <dd className={`mt-4 md:mt-auto mb-4 text-sm md:text-base${c < comments.length - 1 ? ' pb-4 border-b' : ''}`}>
                    {comment.comment}
                </dd>
              </React.Fragment>
            )
          })}
        </dl>
    </Box>
    }
    </article>
  )
}

export default Article