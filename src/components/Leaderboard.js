import React from "react"
import { Box } from "@material-ui/core"
import { makeInitials } from "functions"
import { useSelector } from "react-redux"
import { getComments } from "store/slices/view"

const Leaderboard = () => {
  let counts = {}
  let comments = useSelector(getComments)
  let authors = comments.map(comment => comment.name)

  //go through our array of author names and check for duplicates, iterating where one is found
  authors.forEach((x) => { counts[x] = (counts[x] || 0) + 1 })

  //take our counts Object, full of names and count, and make a new array of names and EMPTY counts
  let leaders = Object.keys(counts).map( ( author, a ) => {
    return { 
      name: author,
      count: Object.values(counts)[a]//Using our index, find the corresponding count from the object
    } 
  })

  leaders.sort((a,b) => {
    return b.count - a.count
  })

  return (
    <aside className="md:sticky md:top-24 w-full md:w-1/3 xl:w-2/5 bg-secondary-200 sticky my-2 md:my-4 lg:my-8 order-1 md:order-2">
      <h2 className="py-2 px-3 sm:p-4 text-md sm:text-xl border-b bg-primary-light font-bold text-white">Top Commentors</h2>
      <Box className="p-2">
      { leaders.map( (leader, l) => {
        //Only show the top 3, so if our index is too high don't return the "leader"
        return (
          <figure key={`leaderboard__leader--${l}`} className={`${l > 2 ? 'hidden ' : ''}flex relative mb-2 shadow bg-white${l%2 ? '-700' : ''}`}>
            <div className="relative bg-slate p-2 sm:p-4 h-16 md:h-20 lg:h-24 w-20 md:w-24 flex justify-center items-center flex-col font-bold text-secondary">
              <span className={`coin ${l === 0 ? 'gold' : ( l > 1 ? 'copper' : 'silver' )} absolute -right-2 -top-2 shadow-sm rounded-full w-8 h-8 p-2 text-sm text-center flex flex-col items-center justify-center`}>
                {l + 1}
              </span>
              <span className="text-xl sm:text-2xl lg:text-3xl">{makeInitials(leader.name)}</span>
            </div>
            <figcaption className="flex flex-col lg:flex-row justify-center md:justify-between items-center w-full px-3 py-2 md:p-4">
              <span className="ml-0 mr-auto lg:mx-4 lg:text-xl 2xl:text-2xl font-bold leading-6">{leader.name}</span>
              <span className="ml-0 mr-auto lg:mr-4 text-sm lg:text-lg 2xl:text-xl uppercase leading-4">{leader.count}&nbsp;Comments</span>
            </figcaption>
          </figure>
        )
      })}
      </Box>
    </aside>
  )
}

export default Leaderboard