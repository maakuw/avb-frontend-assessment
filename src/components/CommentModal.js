import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, Box, Modal, Button, Input } from "@material-ui/core";

import {
  addComment, 
  getComments, 
  closeCommentsModal, 
  getViewCommentsModalOpen,
} from "store/slices/view";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

const CommentModal = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isOpen = useSelector(getViewCommentsModalOpen);
  const comments = useSelector(getComments);

  const handleClose = () => dispatch(closeCommentsModal());

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      className={classes.modal}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description">
      <Box
        component="form" 
        autoComplete="off" 
        onSubmit={(event)=>{
          event.preventDefault()
          const comment = document.getElementById('comment').value
          const name = document.getElementById('full_name').value
          dispatch(addComment({
            id: comments.length + 1, 
            name: name,
            comment: comment
          }))
          handleClose()
        }}
        sx={{
        width: '33vw', 
        maxWidth: '40rem', 
        minWidth: '20rem',
        maxHeight: '80vh', 
        zIndex: 1, 
        padding: '2rem', 
        borderRadius: 10, 
        overflow: 'scroll', 
        bgcolor: 'white'}}>
        <Input 
         id="full_name" 
         fullWidth 
         label="Name" 
         defaultValue="" 
         autoFocus
         placeholder="Enter your first and last name"/>
        <TextField
          id="comment"
          label="Add a comment"
          multiline
          maxRows={4} 
          fullWidth
          defaultValue="" 
          placeholder="Type your comment here..."/>
        <nav className="mt-8 flex justify-between">
          <Button variant="outlined" color="primary" type="cancel" onClick={handleClose}>Cancel</Button>
          <Button variant="contained" color="primary" type="submit">Submit</Button>
        </nav>
      </Box>
    </Modal>
  );
};

export default CommentModal;
