import { createSlice, createSelector } from "@reduxjs/toolkit";
import { mockComments } from "store/api";

export const name = "view";
const initialState = {
  commentsModalOpen: false, 
  comments: mockComments
};

const viewSlice = createSlice({
  name,
  initialState,
  reducers: {
    addComment(state, comment) {
      let comments = state.comments
      comments.unshift(comment.payload)
      state.comments = comments
    },
    openCommentsModal(state) {
      state.commentsModalOpen = true;
    },
    closeCommentsModal(state) {
      state.commentsModalOpen = false;
    },
  },
});

const getSlice = (state) => state[name] || {};

export const getViewCommentsModalOpen = createSelector(
  getSlice,
  (slice) => slice.commentsModalOpen
);

export const getComments = createSelector(
  getSlice,
  (slice) => slice.comments
);

export const { openCommentsModal, closeCommentsModal, addComment } = viewSlice.actions;
export default viewSlice.reducer;
