const colors = require('tailwindcss/colors')

module.exports = {
  future: {
    purgeLayersByDefault: true,
    removeDeprecatedGapUtilities: true,
  },
  theme: {
    extend: {
      animation: {
       'spin': 'spin 20s linear infinite',
      }
    },
    fontFamily: {
      'sans': ['-apple-system', 'BlinkMacSystemFont', "Segoe UI", "Roboto", "Oxygen","Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue","sans-serif"],
      'mono': ['source-code-pro', 'Menlo', 'Monaco', 'Consolas', "Courier New", 'monospace']
    },
    minHeight: {
     '0': '0',
     '30vw': '30vw',
     '30': '30vh',
     'full': '100%',
     'screen': '100vh'
    },
    backgroundColor: theme => ({
      'white': {
        'DEFAULT': 'white',
        '700': 'rgba(255,255,255,0.7)'
      },
      'slate': {
        'DEFAULT':'#282c34',
        '100':'rgba(40,44,52,0.1)',
        '200':'rgba(40,44,52,0.2)'
      },
      'primary': {
        'DEFAULT':'#3f51b5',
        'dark':'#3f51b5',
        'light': 'rgb(121, 134, 203)',
        '100':'rgba(63,81,181,0.1)',
        '200':'rgba(63,81,181,0.2)'
      },
      'secondary': {
        'DEFAULT':'#61dafb',
        '100':'rgba(97,218,251,0.1)',
        '200':'rgba(97,218,251,0.2)'
      }
    }),
    textColor: theme => ({
      'white': 'white', 
      'primary': '#3f51b5',
      'secondary': '#61dafb',
      'slate': '#282c34'
    })
  },
  variants: {
    extend: {},
    animation: ['responsive', 'motion-safe', 'motion-reduce']
  },
  plugins: [], 
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'media', // or 'media' or 'class'
}