
# AVB Frontend Assesment

## User Story

Users are able to view a list of comments as well as a leaderboard of the top 3 commenters from the default view. Clicking/tapping the top right button will trigger a modal overlay, where a user can add a comment.

## Tasks

There was a list of things we were required to do, and plenty of space left to develop as desired. The list of Tasks, as well as how they were executed is below.

### Use the Material-UI theme for color scheme

Once the theme was investigated, the colors from Material were rolled into the Tailwind.config.js to ensure parity. Primary.light is used in the avatar icons. Primary.dark is integrated into a class and used in the header. Secondary was sampled from the logo "teal" color and used for the aside header. The "Slate" color of the hero background is used for headline text, as well as a translucent color for the Leaderboard.

### Display list of comments

The comment list was rolled into an article tag and splayed out on the left side 80% of viewport width. It is fully responsive, moving below the Leaderboard on mobile. The list updates when a comment is added, putting the new comment at the top. Adding comments is dispatched to redux, added to the state, and the new object is returned to the view.

The initial creation is a method added to the functions.js that is used to create avatar copy app-wide. If there is no space in the name, there is no last initial. It doesn't account for an initial space, as this should be handled by input sanitization. The api.js file was imported into the file and used on initial load. That payload is sent through the selector back to the Article. This could be connected to some middleware like reduc-thunk, but I did no continue on to execute the "Extra" part.

### Facilitate adding a comment via modal with input fields(name and comment), and submit button

For UI sake, a label and "Cancel" button were also included in the modal. The comment field is multiline textarea and expands with copy, respectful of carriage returns. Tabs are programmed and initial load sets focus to the "first name" field.

### Display a list of top 3 commenters

A Leaderboard component was created to showcase the top 3 authors. The visual is slightly different, as is the DOM, from the Articale. A figure was used here due to the lack of text relevance to sight impaired. Nevertheless, avatar, number of comments and name is included. There is a badge for each link, colored to their respective level. ALL entries are processed, but only the top 3 are output. The results are updated automatically, so adding a new comment effects the list and counts. The order is decending.

This aside is also responsive, appearing at the top of the view on mobile. On tablet, the comment count drops below the user name.

## Global Changes

1. Material-UI was used where it a) *could* be used without injecting excess DOM and b) where it didn't affect the speed and load time based on [sx parameter being *much* slower than the className attribute and use of styles](https://mui.com/system/basics/#the-sx-prop). In summary, where methods could be leveraged or no faster option was available, Material-UI was used.
2. The "getStyle" was left in the header, but was not used elsewhere in the site for the same reason as above. In the end, Helper CSS is just so much faster and can be leveraged sitewide. In essence, where the style was one-off or didn't have a Tailwind equivelant, I left it alone.
3. Tailwind.css was used globally to rapidly produce this project. The styles are small, the trash is collected on unused styles, I was able to use existing inline styles from the project directly with Tailwind for flexibility. As a result, a few elements were added to the package.json including Craco, which allows npm scripts to run and include tailwind without adding webpack.
4. Autoprefixer was setup to address cross-browser compatibility, but mostly to facilitate Tailwind. It *could* be configured to ensure the CSS is all prefixed, but I didn't double-check to ensure that it was since that wasn't part of the test confines.
5. Lastly, semantic DOM was used in several places where DIVs would normally be spit out by Material-ui. This decreased the number of elements in the page compared to Material while increasing the usability, accessiblity, and SEO relevance of the content therein. The Modal component, however, was done entirely in Material to ensure AVB is aware that I'm capable of it's use, even the older version used in this projecdt.